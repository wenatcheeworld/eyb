#!/bin/bash

set -euf -o pipefail

BUCKET=eyb.ncwsports.com
LOCAL=./site

cd $LOCAL
aws s3 sync . s3://$BUCKET --acl public-read --region us-east-1
