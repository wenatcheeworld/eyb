# Eastmont Youth Baseball

This is a static website hosted in Amazon S3. A script is included to update the
site which uses the [awscli](https://aws.amazon.com/cli/). To run the script, you
will first need to run the following commands:

```
pip install awscli
aws configure
```

The second command will require your API keys. If you don't already have them
setup, see [Amazon's setup page](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-set-up.html).


To update the site, simply run:

```
./upload_to_s3.sh
```

within this directory. Note that it will upload the files as they currently exist
on your filesystem, not necessarily what has been pushed to Bitbucket.
